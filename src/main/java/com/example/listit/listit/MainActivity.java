package com.example.listit.listit;

import android.app.Service;
import android.content.Context;
import android.media.Image;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private LinearLayout parent;
    private EditText ListTitle;
    private EditText ListItem;
    private TextView CharCount;
    private ImageButton Comment;
    private ImageButton Camera;
    private ImageButton Location;
    private  View optionBar;
    private  InputMethodManager imm;
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListItem = (EditText)findViewById(R.id.ListItemEt);
        CharCount = (TextView)findViewById(R.id.textCountTv);
        parent = (LinearLayout) findViewById(R.id.parentlayout);
        Comment =(ImageButton)findViewById(R.id.commentib);
        Camera =(ImageButton)findViewById(R.id.cameraib);
        Location =(ImageButton)findViewById(R.id.locationib);
         final TextWatcher mTextEditorWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                CharCount.setText(String.valueOf(300 - s.length()));
                Comment.setImageResource(R.drawable.compose_comment_default);
                Camera.setImageResource(R.drawable.compose_camera_default);
                Location.setImageResource(R.drawable.compose_location_default);
            }

            public void afterTextChanged(Editable s) {
                if(s.length()==0){
                    Comment.setImageResource(R.drawable.compose_comment_disabled);
                    Camera.setImageResource(R.drawable.compose_camera_disabled);
                    Location.setImageResource(R.drawable.compose_location_disabled);
                }
            }
        };

        ListItem.addTextChangedListener(mTextEditorWatcher);





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
